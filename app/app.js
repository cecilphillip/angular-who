(function () {
    'use strict';

    angular.module('app', ['ngRoute', 'ngAnimate', 'ngSanitize', 'mgcrea.ngStrap']);

    angular.module('app')
        .constant('notifier', toastr);

    angular.module('app')
        .config(['$routeProvider', function ($routeProvider) {
        $routeProvider
			.when('/', {
			    templateUrl: 'app/templates/home.html',
			    controller: 'homeController'
			})
            .when('/edit', {
                templateUrl: 'app/templates/edit.html',
                controller: 'editController'
            })
           .otherwise({
               redirectTo: '/'
           });
    }]);
}());
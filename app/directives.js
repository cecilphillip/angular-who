(function() {
    angular.module('app')
        .directive('confirmButton', confirmButton);

    function confirmButton() {
        return {
            restrict: 'A',
            link: function (scope, ele, attrs) {
                var options = scope.$eval(attrs.confirmButton);

                //setup popover options
                var buttonId = Math.floor(Math.random() * 10000000000),
                    message = (options && options.message) || "Are you sure?",
                    title = (options && options.title) || "Confirm",
                    yesText = (options && options.yesText) || "Yes",
                    noText = (options && options.noText) || "No",
                    container = (options && options.container) || false,
                    placement = (options && options.placement) || "bottom",
                    html;

                html = "<div id=\"confirmPopover-" + buttonId + "\">\n  <span class=\"confirmbutton-msg\">" + message +
                    "</span><br>\n	<button class=\"confirmbutton-yes btn btn-danger\"> " +
                    " <span class=\"glyphicon glyphicon-thumbs-up\"></span>  " + yesText +
                    "</button>\n	<button class=\"confirmbutton-no btn\">" +
                    " <span class=\"glyphicon glyphicon-thumbs-down\"></span>  " + noText + "</button>\n</div>";

                //attched bootstrap popover
                ele.popover({
                    content: html,
                    container: container,
                    html: true,
                    trigger: "manual",
                    title: title,
                    placement: placement
                });

                return ele.bind('click', function (e) {
                    e.stopPropagation();
                    var opts = scope.$eval(attrs.confirmButton);
                    var passthrough = (opts && opts.passthrough) || false;
                    if (passthrough) {
                        scope.$apply(attrs.confirmAction);
                        return;
                    }
                    ele.popover('show');
                    var pop = $("#confirmPopover-" + buttonId);

                    pop.find('.confirmbutton-yes').click(function (e) {
                        //
                        scope.$apply(attrs.confirmAction);
                        ele.popover('hide');
                    });

                    pop.find('.confirmbutton-no').click(function (e) {
                        ele.popover('hide');
                    });
                });
            }
        };
    };
}());
(function () {
    'use strict';

    //#region navbar controller
    angular.module('app')
       .controller('navbarController', ['$scope', '$location', navbarController]);

    function navbarController($scope, $location) {
        $scope.activateNavItem = function (location) {
            return location === $location.path();
        };
    }
    //#endregion

    //#region home controller
    angular.module('app')
        .controller('homeController', ['$scope', 'usStatesService', 'notifier', homeController]);

    function homeController($scope, usStatesService, notifier) {
        $scope.loadStates = function () {
            usStatesService.getStates().then(function (data) {
                $scope.usStates = data;
                notifier.success('States Loaded');
            });
        };

        $scope.clearStates = function () {
            $scope.usStates = [];
            notifier.info('States Loaded');
        };
    }
    //#endregion

    //#region edit controller
    angular.module('app')
        .controller('editController', ['$scope', '$modal', editController]);

    function editController($scope, $modal) {
        $scope.submit = function () {
            var message = "<div>User: " + $scope.userEmail + ' <br />Password:' + $scope.userPassword + '</div>';

            $modal({
                scope: $scope,
                content: message,
                html: true,
                show: true,
                keyboard: false,
                backdrop: 'static',
                placement: 'top',
                animation: 'am-fade-and-scale'
            });

        };
    }
    //#endregion
}());